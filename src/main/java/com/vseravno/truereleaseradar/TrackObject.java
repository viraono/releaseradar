package com.vseravno.truereleaseradar;

import com.google.auto.value.AutoValue;

import javax.annotation.Nullable;

@AutoValue
public abstract class TrackObject {
  public abstract String trackSid();

  public abstract String trackName();

  @Nullable
  public abstract String releaseDate();

  public static TrackObject create(String trackSid, String trackName) {
    return new AutoValue_TrackObject(trackSid, trackName, null);
  }

  public static TrackObject create(String trackSid, String trackName, String releaseDate) {
    return new AutoValue_TrackObject(trackSid, trackName, releaseDate);
  }

  public String getUri() {
    return "spotify:track:" + trackSid();
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (o instanceof TrackObject) {
      TrackObject that = (TrackObject) o;
      return trackSid().equals(that.trackSid()) && trackName().equals(that.trackName());
    }
    return false;
  }

  @Override
  public int hashCode() {
    int h$ = 1;
    h$ *= 1000003;
    h$ ^= trackSid().hashCode();
    h$ *= 1000003;
    h$ ^= trackName().hashCode();
    return h$;
  }
}
