package com.vseravno.truereleaseradar;

import com.vseravno.truereleaseradar.db.TrackService;
import com.wrapper.spotify.SpotifyApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.Set;

@RestController
public class Controller {
  private final SpotifyApi spotifyApi;
  private final TrackService trackService;
  private final SpotifyService spotifyService;

  @Autowired
  public Controller(
      SpotifyApi spotifyApi, TrackService trackService, SpotifyService spotifyService) {
    this.spotifyApi = spotifyApi;
    this.trackService = trackService;
    this.spotifyService = spotifyService;
  }

  @GetMapping("/update")
  void update() {
    spotifyService.refreshAccessToken();
    Timestamp lastPlaylistUpdateDate = trackService.getLastInsertionDate();
    Manager manager = new Manager(spotifyApi, spotifyService, lastPlaylistUpdateDate);
    Set<TrackObject> newTracks = manager.getNewTracks(trackService.getTracks());
    manager.addTracks(newTracks);
    trackService.updateTracks(newTracks);
  }
}
