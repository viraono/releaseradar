package com.vseravno.truereleaseradar.db;

import com.vseravno.truereleaseradar.TrackObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.PageRequest;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Set;

@Service
public class TrackService {
  private final TrackRepository trackRepository;

  @Autowired
  public TrackService(TrackRepository trackRepository) {
    this.trackRepository = trackRepository;
  }

  public void updateTracks(Set<TrackObject> allTracks) {
    for (TrackObject tr : allTracks) {
      Track track = new Track();
      track.setTrackSid(tr.trackSid());
      track.setInserted(Timestamp.from(Instant.now()));
      trackRepository.save(track);
    }
  }

  public List<Track> getTracks() {
    return trackRepository.getAllTracks();
  }

  public Timestamp getLastInsertionDate() {
    List<Track> latestInsertedTrack = trackRepository.findLatestInsertedTrack(PageRequest.of(0, 1));
    Track latestTrack = latestInsertedTrack.get(0);
    return latestTrack.getInserted();
  }
}
