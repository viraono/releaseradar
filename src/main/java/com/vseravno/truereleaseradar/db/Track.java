package com.vseravno.truereleaseradar.db;

import com.google.common.base.Objects;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "track")
public class Track {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(name="track_sid")
  private String trackSid;

  private Timestamp inserted;

  public Track() {}

  public Track(String trackSid, Timestamp inserted) {
    this.trackSid = trackSid;
    this.inserted = inserted;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getTrackSid() {
    return trackSid;
  }

  public void setTrackSid(String trackSid) {
    this.trackSid = trackSid;
  }

  public Timestamp getInserted() {
    return inserted;
  }

  public void setInserted(Timestamp inserted) {
    this.inserted = inserted;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Track track = (Track) o;
    return id == track.id &&
        Objects.equal(trackSid, track.trackSid);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id, trackSid);
  }

  @Override
  public String toString() {
    return "Track{" +
        "id=" + id +
        ", trackSid='" + trackSid +
        '}';
  }
}
