package com.vseravno.truereleaseradar.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Repository
public interface TrackRepository extends JpaRepository<Track, Long> {

  @Query("SELECT t FROM Track t")
  List<Track> getAllTracks();

  @Query("SELECT t FROM Track t ORDER BY t.inserted DESC")
  List<Track> findLatestInsertedTrack(Pageable pageable);
}
