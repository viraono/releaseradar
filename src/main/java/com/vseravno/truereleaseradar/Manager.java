package com.vseravno.truereleaseradar;

import com.google.common.collect.Lists;
import com.google.common.util.concurrent.RateLimiter;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.model_objects.specification.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Manager {
  private static final Logger logger = LoggerFactory.getLogger(Manager.class);
  private static final int TRACK_LIMIT = 100;
  private static final int FETCH_LIMIT = 50;
  private final SpotifyApi api;
  private final SpotifyService spotifyService;
  private Timestamp lastPlaylistUpdateDate;

  private static final List<String> POP_ARTISTS =
      List.of(
          "Avicii",
          "Selena Gomez",
          "Dua Lipa",
          "Coldplay",
          "Tiësto",
          "David Guetta",
          "Ed Sheeran",
          "Mariah Carey",
          "ABBA",
          "Imagine Dragons",
          "Boney M.",
          "Elvis Presley",
          "Fall Out Boy",
          "Linkin Park",
          "Green Day",
          "Timmy Trumpet",
          "Gunna",
          "Jason Derulo",
          "Swedish House Mafia",
          "Post Malone",
          "Alesso",
          "Troye Sivan",
          "MEDUZA",
          "Imanbek",
          "Sam Smith",
          "SZA",
          "Tony Romera",
          "Cristian Ferrer",
          "Ben Böhmer");

  private static final List<String> EXCLUDED_ALBUMS =
      List.of(
          "Group Therapy",
          "workout",
          "anjuna",
          "toolroom",
          "ibiza",
          "tech house",
          "Acoustic Covers");

  static RateLimiter rateLimiter = RateLimiter.create(5);

  @Autowired
  public Manager(SpotifyApi api, SpotifyService spotifyService, Timestamp lastPlaylistUpdateDate) {
    this.api = api;
    this.spotifyService = spotifyService;
    this.lastPlaylistUpdateDate = lastPlaylistUpdateDate;
  }

  // TODO write the method
  public void updateDb(List<com.vseravno.truereleaseradar.db.Track> dbFetch) {}

  // TODO rewrite logic, change sid to name+artist
  public Set<TrackObject> getNewTracks(List<com.vseravno.truereleaseradar.db.Track> dbFetch) {
    Set<String> dbSids =
        dbFetch.stream()
            .map(com.vseravno.truereleaseradar.db.Track::getTrackSid)
            .collect(Collectors.toSet());

    Set<TrackObject> newTracks = prepareTracks();
    return newTracks.stream()
        .filter(t -> !dbSids.contains(t.trackSid()))
        .collect(Collectors.toSet());
  }

  public void addTracks(Set<TrackObject> tracks) {
    Lists.partition(Lists.newArrayList(tracks), TRACK_LIMIT)
        .forEach(
            tracksBatch -> {
              try {
                spotifyService.addTracksToPlaylist(tracksBatch);
                // just in case
                Thread.sleep(2000);
              } catch (InterruptedException e) {
                logger.error("Error when adding tracks to the playlist", e);
              }
            });
  }

  private Set<TrackObject> prepareTracks() {
    HashSet<TrackObject> allTracks = new HashSet<>();
    List<Artist> artists = spotifyService.getFollowedArtists();
    List<AlbumSimplified> newalbums = new ArrayList<>();
    logger.info("Checking every single one of your {} artists", artists.size());
    for (Artist artist : artists) {
      try {
        Set<AlbumSimplified> currentAlbums = findNewAlbums(artist.getId());
        newalbums.addAll(currentAlbums);
        rateLimiter.acquire();
        for (AlbumSimplified currentAlbum : currentAlbums) {
          logger.info(
              "Found a new release {} from artist {}", currentAlbum.getName(), artist.getName());
        }
      } catch (Exception e) {
        logger.warn("Failed to process artist {}", artist.getName(), e);
      }
    }
    Set<TrackSimplified> newTracks = getTracks(newalbums);
    newTracks.stream()
        .map(
            track ->
                TrackObject.create(track.getId(), track.getName(), trackArtistsToString(track)))
        .forEach(allTracks::add);
    return allTracks;
  }

  private Set<TrackSimplified> getTracks(List<AlbumSimplified> albums) {
    Set<TrackSimplified> newTracks = new HashSet<>();
    for (AlbumSimplified album : albums) {
      List<TrackSimplified> albumTracks = spotifyService.getAlbumTracks(album.getId());
      // filter compilation albums
      if (!isPopCompilation(albumTracks)) {
        newTracks.addAll(albumTracks);
      } else {
        logger.info("Pop compilation {} successfully eliminated", album.getName());
      }
    }

    Map<String, TrackSimplified> noDublicates =
        newTracks.stream().collect(Collectors.toMap(t -> trackHash(t), t -> t, (t1, t2) -> t1));

    return new HashSet<>(noDublicates.values());
  }

  private boolean isPopCompilation(List<TrackSimplified> albumTracks) {
    Stream<ArtistSimplified> artists =
        albumTracks.stream().flatMap(tr -> Arrays.stream(tr.getArtists()));
    return artists.anyMatch(artist -> POP_ARTISTS.contains(artist.getName()));
  }

  private String trackHash(TrackSimplified track) {
    ArtistSimplified[] artists = track.getArtists();
    String name = track.getName();
    String collect =
        Stream.of(artists).map(ArtistSimplified::getId).sorted().collect(Collectors.joining());
    return name + collect;
  }

  private Set<AlbumSimplified> findNewAlbums(String artistSid) {
    List<AlbumSimplified> albums = spotifyService.getArtistAlbums(artistSid);
    List<AlbumSimplified> albumsWithFullDate =
        albums.stream()
            .filter(album -> album.getReleaseDate().length() == 10)
            .collect(Collectors.toList());
    Set<AlbumSimplified> newAlbums =
        albumsWithFullDate.stream()
            .filter(alb -> EXCLUDED_ALBUMS.stream()
                .noneMatch(excluded -> alb.getName().toLowerCase().contains(excluded.toLowerCase())))
            .filter(this::newAlbums)
            .collect(Collectors.toSet());
    Collection<AlbumSimplified> uniqueAlbums =
        newAlbums.stream()
            .collect(Collectors.toMap(AlbumSimplified::getName, a -> a, (a1, a2) -> a1))
            .values();
    return Set.copyOf(uniqueAlbums);
  }

  private boolean newAlbums(AlbumSimplified album) {
    LocalDate currentDate = LocalDate.now();
    LocalDate albumDate = LocalDate.parse(album.getReleaseDate());
    LocalDate lastUpdateDate = lastPlaylistUpdateDate.toLocalDateTime().toLocalDate();
    // TODO name variable better ?
    long neededReleasePeriod = ChronoUnit.DAYS.between(lastUpdateDate, currentDate);
    long releasePeriod = ChronoUnit.DAYS.between(albumDate, currentDate);
    return releasePeriod <= neededReleasePeriod;
  }

  private String trackArtistsToString(TrackSimplified track) {
    ArtistSimplified[] artists = track.getArtists();
    String trackArtists =
        Arrays.stream(artists).map(ArtistSimplified::getName).collect(Collectors.joining(","));
    return trackArtists;
  }
}
