package com.vseravno.truereleaseradar;

import com.google.gson.JsonArray;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.enums.ModelObjectType;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.exceptions.detailed.NotFoundException;
import com.wrapper.spotify.model_objects.credentials.AuthorizationCodeCredentials;
import com.wrapper.spotify.model_objects.specification.*;
import org.apache.hc.core5.http.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class SpotifyService {
  private final String playlistSid = "31Z48YMvURcqwR4JIZALE6";
  private static final Logger logger = LoggerFactory.getLogger(SpotifyService.class);
  private static final int FETCH_LIMIT = 50;
  private final SpotifyApi api;

  @Autowired
  public SpotifyService(SpotifyApi api) {
    this.api = api;
  }

  public void refreshAccessToken() {
    try {
      AuthorizationCodeCredentials tokens = api.authorizationCodeRefresh().build().execute();
      api.setAccessToken(tokens.getAccessToken());
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      logger.error("Error refreshing Spotify access token", e);
      throw new RuntimeException();
    }
  }

  public List<Artist> getFollowedArtists() {
    try {
      List<Artist> artists = new ArrayList<>();
      // initial fetch of followed artists
      PagingCursorbased<Artist> artistsPaging =
          api.getUsersFollowedArtists(ModelObjectType.ARTIST).limit(FETCH_LIMIT).build().execute();
      artists.addAll(Arrays.asList(artistsPaging.getItems()));
      // continue fetching artists
      while (true) {
        Artist artist = artists.get(artists.size() - 1);
        String id = artist.getId();
        PagingCursorbased<Artist> moreArtistsPaging =
            api.getUsersFollowedArtists(ModelObjectType.ARTIST).after(id).build().execute();
        List<Artist> moreArtists = Arrays.asList(moreArtistsPaging.getItems());
        if (moreArtists.isEmpty()) {
          break;
        }
        artists.addAll(moreArtists);
      }
      return artists;
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      throw new RuntimeException(e);
    }
  }

  public List<AlbumSimplified> getArtistAlbums(String artistSid) {
    List<AlbumSimplified> albums = new ArrayList<>();
    try {
      Paging<AlbumSimplified> albumsPaging =
          api.getArtistsAlbums(artistSid)
              .album_type("album,single")
              .limit(FETCH_LIMIT)
              .build()
              .execute();
      Integer total = albumsPaging.getTotal();
      albums.addAll(Arrays.asList(albumsPaging.getItems()));

      for (int offset = FETCH_LIMIT; offset < total; offset += FETCH_LIMIT) {
        Paging<AlbumSimplified> moreAlbums =
            api.getArtistsAlbums(artistSid).offset(offset).build().execute();
        albums.addAll(Arrays.asList(moreAlbums.getItems()));
      }
      return albums;
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    } catch (SpotifyWebApiException | ParseException e) {
      throw new RuntimeException(e);
    }
  }

  public List<TrackSimplified> getAlbumTracks(String albumSid) {
    try {
      List<TrackSimplified> tracks = new ArrayList<>();
      Paging<TrackSimplified> pagingTracks =
          api.getAlbumsTracks(albumSid).limit(FETCH_LIMIT).build().execute();
      tracks.addAll(Arrays.asList(pagingTracks.getItems()));
      return tracks;
    } catch (NotFoundException e) {
      // in case of NotFoundException (non-existing id), skip the album
      logger.warn("Failed to get album with Spotify id {}", albumSid);
      return List.of();
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      throw new RuntimeException(e);
    }
  }

  public void addTracksToPlaylist(List<TrackObject> tracks) {
    JsonArray jsonUris = new JsonArray();
    for (TrackObject track : tracks) {
      jsonUris.add(track.getUri());
    }
    try {
      api.addItemsToPlaylist(playlistSid, jsonUris).build().execute();
      logger.info("{} new tracks added.", tracks.size());
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      throw new RuntimeException();
    }
  }
}
